## Technology

**Alamofire библиотека для запроса данных из сети** 

**Поиск треков через iTunes API** 

**AVKit Audio Player для воспроизведения музыкальных файлов** 

**SDWebImage(UIKit) и URLImage(SwiftUI) библиотеки для загрузки изображений** 

**Анимированные переходы через Auto Layout**

**Работа с UIPanGestureRecognizer(UIKit), LongPressGesture(SwiftUI)** 

**UIKit и SwiftUI в рамках одного проекта**

**Перетаскиваемый Track Detail Player доступен между всеми экранами приложения**

**Декодирование JSON ответа в асинхронном потоке**

**Архитектуру Clean Swift**
